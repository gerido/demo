package com.second.module;

public class DemoClass {

    public int calculateSomething(int number) {
        int result = number + 2;
        number = number + 1;
        result *= number;
        return result;
    }
}
